import React, { useState, useEffect } from "react";
import { Link } from 'react-router-dom';
import './Home.css'
import { Row, Button, Container, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import swal from 'sweetalert';
import UserService from '../../services/index';
import CardDetails from '../../components/Card/Card'
import TitleCards from "../../components/TitleCard/TitleCard";
import Details from '../../components/Details/Details';
import SeatArrangement from '../../components/SeatArrangement/SeatArrangement';

const Home = (props) => {

	const [busDetails, setBusDetails] = useState([]);
	const [modal, setModal] = useState(false);
	const [seats, setSeats] = useState([]);
	const [busId, setBusId] = useState("");
	const [bus, setBus] = useState(false);
	const [fare, setFare] = useState("");
	const [gender, setGender] = useState("Male");
	const [fareConstant, setFareConstant] = useState("");
	const [seatArrangement, setSeatArrangement] = useState([]);
	const [name, setName] = useState("");
	const [email, setEmail] = useState("");
	const [mobileNumber, setMobileNumber] = useState("");
	const [age, setAge] = useState("");
	const [data,setData] = useState({});

	useEffect(async () => {
		window.scrollTo(0, 0);
	}, []);

	
	const genderChanged = (e) => {
		const gender = e.target.value;
		setGender(gender);
	}
	function convertDate(dateString) {
		var p = dateString.split(/\D/g)
		return [p[2], p[1], p[0]].join("/")
	}

	const checkAvailability = async (sourceCity,destinationCity,travelDate) => {

		if (!sourceCity || !destinationCity || !travelDate) {
			swal({
				title: 'Please enter all fields to proceed',
				icon: 'warning',
				timer: 2000,
				buttons: true,
			})
		}
		if (sourceCity === destinationCity) {
			swal({
				title: 'Source and Destination City cannot be same:)',
				icon: 'warning',
				timer: 2000,
				buttons: true,
			})
		}
		else {
			let date = convertDate(travelDate);
			UserService.getBusDetails(sourceCity, destinationCity, date).then(
				async (response) => {
					console.log(response)
					await setBusDetails(response.data);
					setBus(true);
					window.scrollTo(0, 950);
				},
				(error) => {
					swal({
						title: 'Something went wrong, please try again',
						icon: 'error',
						timer: 2000,
						buttons: true,
					})
				}
			);
		}
	}

	const onchangeSeat = async (e) => {
		if (seats.length < 20) {
			if (seats.includes(e)) {
				for (var i = 0; i < seats.length; i++) {
					if (seats[i] === e) {
						seats.splice(i, 1);
						setFare(fare - fareConstant);
					}
				}
			}
			else {
				seats.push(e);
				setFare(seats.length * fareConstant);
			}
		}
		else {
			swal({
				title: 'User can select 20 seats per registration',
				icon: 'warning',
				timer: 2000,
				buttons: true,
			})
		}
	}
	
	const openModal = async (seats, id, fare) => {
		setBusId(id); setSeatArrangement(seats); setFare(fare);
		setFareConstant(fare);
		setModal(true);
	}
	const closeModal = async () => {
		setSeats([]);
		setFare("");
		setGender("Male");
		setModal(false);
	}
	const onChangeName = (e) => {
		const name = e.target.value;
		setName(name);
	};
	const onChangeEmail = (e) => {
		const email = e.target.value;
		setEmail(email);
	};
	const onChangeMobileNumber = (e) => {
		const mobileNumber = e.target.value;
		setMobileNumber(mobileNumber);
	};
	const onChangeAge = (e) => {
		const age = e.target.value;
		setAge(age);
	};
	const bookSeats = async (e) => {
		if (!name || !mobileNumber || !email || !age || !gender) {
			swal({
				title: 'Please enter all fields to proceed',
				icon: 'warning',
				timer: 2000,
				buttons: true,
			})
		}
		else {
			 UserService.bookSeats(name, mobileNumber, email, age, gender, busId, seats).then(
				async (response) => {
					swal({
						title: 'You seat has been booked, Please check your mail for futher details',
						icon: 'success',
						timer: 4000,
						buttons: true,
					})
					setSeats([]);
					setFare("");
					setGender("Male");
					setModal(false);
					setAge("");
					setName("");
					setMobileNumber("");
					setEmail("");
				},
				(error) => {
					alert("Something went wrong, please try again");
					setSeats([]);
					setFare("");
					setGender("Male");
					setModal(false);
					setAge("");
					setName("");
					setMobileNumber("");
					setEmail("");
				}
			);
		}
	}
	return (
		<body>
			<div id="booking" class="section" >
				<div class="section-center" >
					<TitleCards checkAvailability={checkAvailability}/>
					<center>
						<div class="booking-for" >
							<div class="col-md-4" >
								<div class="form-btn">
									<button class="submit-btn" ><Link to="/ticketStatus" style={{'color':'white'}} >View Ticket Status</Link></button>
								</div>
							</div>
						</div>
					</center>
				</div>
			</div>
			{bus ?
				<Container style={{'paddingBottom':'100px'}}>
					<center>	
					<p className="bus-found" >{busDetails.length} buses found</p></center>
						{busDetails.map((value, index) => (
							  <CardDetails value={value} busDetails={openModal} />
						))}
					<Modal isOpen={modal} toggle={closeModal} size="xl" backdrop={false}>
						<ModalHeader xl={12} lg={12} md={12} toggle={closeModal}>View Passenger Seats</ModalHeader>
						<ModalBody>
							<Row>
								<SeatArrangement seatArrangement={seatArrangement} onchangeSeat={onchangeSeat}/>
								{seats.length > 0 ?
										<Details seats={seats} fare={fare} name={name} email={email} mobileNumber={mobileNumber} gender={gender} age={age}
										onChangeName={onChangeName} onChangeEmail={onChangeEmail} onChangeMobileNumber={onChangeMobileNumber}
										onChangeAge={onChangeAge} genderChanged={genderChanged}
										/>								
									: null}
							</Row>
							<center>
								<Button color='primary' className="reset-div" onClick={bookSeats}>Book seat Now</Button>
							</center>
						</ModalBody>
						<ModalFooter>
							<Button color='secondary' onClick={closeModal}>Cancel</Button>
						</ModalFooter>
					</Modal>
				</Container>
				:
				null}
		</body>
	);
};

export default Home;
