import React, { useState } from "react";
import "./TitleCard.css";
import data from "../../constants/cities-name-list.json";
import { Col, Row, Container } from "../../../node_modules/reactstrap";
import TitleCard from "../TitleCard/TitleCard";

function TitleCards(props) {
  const [sourceCity, setSourceCity] = useState("Chennai");
  const [destinationCity, setDestinationCity] = useState("Coimbatore");
  const [travelDate, setTravelDate] = useState("22/02/2021");

  const onChangeSourceCity = (e) => {
    const sourceCity = e.target.value;
    setSourceCity(sourceCity);
  };
  const onChangeDestinationCity = (e) => {
    const destinationCity = e.target.value;
    setDestinationCity(destinationCity);
  };
  const onChangeTravelDate = (e) => {
    const travelDate = e.target.value;
    setTravelDate(travelDate);
  };
  function checkAvailability() {
    props.checkAvailability(sourceCity, destinationCity, travelDate);
  }
  return (
    <Container>
      <div class="booking-form">
        {" "}
        <center>
          <p className="welcome">Welcome to HYPERBUS</p>
        </center>
        <Row>
          <Col md={6} sm={6} xs={6}>
            <div class="form-group">
              <select
                class="form-control"
                value={sourceCity}
                onChange={onChangeSourceCity}
              >
                {data.map((value, index) => (
                  <option value={value}>{value}</option>
                ))}
              </select>
              <span class="select-arrow"></span>
              <span class="form-label">From</span>
            </div>
          </Col>
          <Col md={6} sm={6} xs={6}>
            <div class="form-group">
              <select
                class="form-control"
                value={destinationCity}
                onChange={onChangeDestinationCity}
              >
                {data.map((value, index) => (
                  <option value={value}>{value}</option>
                ))}
              </select>
              <span class="select-arrow"></span>
              <span class="form-label">To</span>
            </div>
          </Col>
          <Col md={6} sm={6} xs={6}>
            <div class="form-group">
              <input
                class="form-control"
                type="date"
                value={travelDate}
                min="2021-01-27"
                onChange={onChangeTravelDate}
                required={true}
              />

              <span class="form-label">Date</span>
            </div>
          </Col>
          <Col md={2} sm={2} xs={2}></Col>
          <Col md={4} sm={4} xs={4}>
            <div class="form-btn">
              <button class="submit-btn" onClick={checkAvailability}>
                Check availability
              </button>
            </div>
          </Col>
        </Row>
      </div>
    </Container>
  );
}
export default TitleCards;
