import React from "react";
import "./SeatArrangement.scss";
import { Col, Row } from "../../../node_modules/reactstrap";
function SeatArrangement(props) {
  function onchangeSeat(id) {
    props.onchangeSeat(id);
  }
  return (
    <Col xl={6} lg={6} md={12}>
      <div className="seat-div">
        <div class="train">
          <li class="row row--1">
            <ol class="seats">
              <Row xs={4}>
                {props.seatArrangement.map((value, index) => (
                  <Col>
                    <li class="seat">
                      <input
                        type="checkbox"
                        id={index}
                        disabled={value.isReserved}
                        onChange={() => onchangeSeat(value.id)}
                      />
                      <label for={index}>
                        &nbsp;&nbsp;&nbsp;{value.id}&nbsp;&nbsp;&nbsp;
                      </label>
                    </li>
                  </Col>
                ))}
              </Row>
            </ol>
          </li>
        </div>
        <div class="exit exit--back train-body"></div>
      </div>
    </Col>
  );
}
export default SeatArrangement;
