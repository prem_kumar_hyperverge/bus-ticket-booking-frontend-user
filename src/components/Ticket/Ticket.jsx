import React, { useState } from "react";
import UserService from "../../services/index";
import './Ticket.css';
import swal from 'sweetalert';
import { Link } from 'react-router-dom';
import { Col, Row } from 'reactstrap';

const Ticket = () => {

  const [name, setName] = useState("");
  const [sourceCity, setSourceCity] = useState("");
  const [destinationCity, setDestinationCity] = useState("");
  const [departureTime, setDepatureTime] = useState("");
  const [pickupLocation, setPickupLocation] = useState("");
  const [dropLocation, setDropLocation] = useState("");
  const [busNumber, setBusNumber] = useState("");
  const [busName, setBusName] = useState('');
  const [travelDate, setTravelDate] = useState("");
  const [seatNumber, setSeatNumber] = useState("");
  const [viewTicket, setViewTicket] = useState(false);
  const [view, setView] = useState(false);
  const [ticketId, setTicketId] = useState("");


  const getDetails = async () => {
    if (!ticketId) {
      swal({
				title: 'Please enter all fields to proceed',
				icon: 'warning',
				timer: 2000,
				buttons: true,
			})
    }
    UserService.ticketDetails(ticketId).then(
      async (response) => {
        if (response.data.data.length == 1) {
          setName(response.data.data[0].name);
          setSeatNumber(response.data.data[0].seatId);
           UserService.busDetails(response.data.data[0].busId).then(
            async (response) => {
              setSourceCity(response.data.data.sourceCity);
              setDestinationCity(response.data.data.destinationCity);
              setPickupLocation(response.data.data.pickupLocation);
              setDropLocation(response.data.data.dropLocation);
              setBusNumber(response.data.data.busNumber);
              setDepatureTime(response.data.data.departureTime);
              setBusName(response.data.data.busName);
              setTravelDate(response.data.data.travelDate);
              setView(true);
              setViewTicket(true);

            },
            (error) => {
              swal({
                title: "Something went wrong, please reload the page",
                icon: 'error',
                timer: 2000,
                buttons: true,
              })
            }
          );
        }
        else {
          setView(true);
          setViewTicket(false);
        }
      },
      (error) => {
        swal({
          title: "Something went wrong, please reload the page",
          icon: 'error',
          timer: 2000,
          buttons: true,
        })
      }

    );
  }
  const onChangeTicketId = (e) => {
    const ticketId = e.target.value;
    setTicketId(ticketId);
    setViewTicket(false);
    setView(false);
  };

  return (
    <div>
      <div class="container1">
        <div class="booking-form" > <center>
          <p className="ticket-status">Ticket Status</p>
        </center>
          <Row>
            <Col md={8} sm={8} xs={8} >
              <div class="form-group">
                <span class="form-label">Ticket ID</span>
                <input class="form-control" type="text" placeholder="Enter your Ticket ID"
                  value={ticketId} onChange={onChangeTicketId}
                />
              </div>
            </Col>
            <Col md={4} sm={4} xs={4} >
              <div class="form-btn">
                <button class="submit-btn" onClick={getDetails} >Search</button>
              </div>
            </Col>
          </Row>
        </div>
        {view ?
          <div>
            {viewTicket ?
              <div>
                <h1>{busName}</h1>
                <ul>
                  <li class="f_row"> <span class="moscow" data={pickupLocation}>{sourceCity}  </span>
                    <svg class="arrow" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512">
                      <path fill="currentColor" d="M224.3 273l-136 136c-9.4 9.4-24.6 9.4-33.9 0l-22.6-22.6c-9.4-9.4-9.4-24.6 0-33.9l96.4-96.4-96.4-96.4c-9.4-9.4-9.4-24.6 0-33.9L54.3 103c9.4-9.4 24.6-9.4 33.9 0l136 136c9.5 9.4 9.5 24.6.1 34z"></path>
                    </svg><span class="san" data={dropLocation} style={{ marginLeft: '10px' }}>{destinationCity}</span>
                  </li>
                  <li class="s_row"> <span class="flight" data="Bus No">{busNumber}</span><span class="seat" data="Seat">{seatNumber}</span></li>
                  <li class="t_row"> <span class="date" data="Date">{travelDate}</span><span class="boarding" data="Boarding">{departureTime}</span></li>
                  <li class="fo_row"> <span class="passenger" data="Passenger">{name}</span></li>
                  <li class="fi_row">
                    <svg class="barcode" xmlns="http://www.w3.org/2000/svg">
                      <path d="M6.834 11.549H1a1 1 0 0 0-1 1v66.236a1 1 0 0 0 1 1h5.834a1 1 0 0 0 1-1V12.549a1 1 0 0 0-1-1zM62.043 11.549h-4.168a1 1 0 0 0-1 1v66.236a1 1 0 0 0 1 1h4.168a1 1 0 0 0 1-1V12.549a1 1 0 0 0-1-1zM17 11.549h-4a1 1 0 0 0-1 1v66.236a1 1 0 0 0 1 1h4a1 1 0 0 0 1-1V12.549a1 1 0 0 0-1-1zM90.334 11.549h-4a1 1 0 0 0-1 1v66.236a1 1 0 0 0 1 1h4a1 1 0 0 0 1-1V12.549a1 1 0 0 0-1-1zM81.167 11.549h-2.724a1 1 0 0 0-1 1v66.236a1 1 0 0 0 1 1h2.724a1 1 0 0 0 1-1V12.549a1 1 0 0 0-1-1zM51.875 11.549h-4a1 1 0 0 0-1 1v66.236a1 1 0 0 0 1 1h4a1 1 0 0 0 1-1V12.549a1 1 0 0 0-1-1zM42.167 11.549h-2.5a1 1 0 0 0-1 1v66.236a1 1 0 0 0 1 1h2.5a1 1 0 0 0 1-1V12.549a1 1 0 0 0-1-1zM73.523 11.549H71.98a1 1 0 0 0-1 1v66.236a1 1 0 0 0 1 1h1.543a1 1 0 0 0 1-1V12.549a1 1 0 0 0-1-1zM33.667 11.549h-4a1 1 0 0 0-1 1v66.236a1 1 0 0 0 1 1h4a1 1 0 0 0 1-1V12.549a1 1 0 0 0-1-1zM23.667 11.549h-1a1 1 0 0 0-1 1v66.236a1 1 0 0 0 1 1h1a1 1 0 0 0 1-1V12.549a1 1 0 0 0-1-1zM67.227 11.549h-.363c-.551 0-1 .448-1 1v66.236c0 .552.449 1 1 1h.363c.551 0 1-.448 1-1V12.549c0-.552-.45-1-1-1z"></path>
                    </svg>
                    <svg class="barcode" xmlns="http://www.w3.org/2000/svg">
                      <path d="M6.834 11.549H1a1 1 0 0 0-1 1v66.236a1 1 0 0 0 1 1h5.834a1 1 0 0 0 1-1V12.549a1 1 0 0 0-1-1zM62.043 11.549h-4.168a1 1 0 0 0-1 1v66.236a1 1 0 0 0 1 1h4.168a1 1 0 0 0 1-1V12.549a1 1 0 0 0-1-1zM17 11.549h-4a1 1 0 0 0-1 1v66.236a1 1 0 0 0 1 1h4a1 1 0 0 0 1-1V12.549a1 1 0 0 0-1-1zM90.334 11.549h-4a1 1 0 0 0-1 1v66.236a1 1 0 0 0 1 1h4a1 1 0 0 0 1-1V12.549a1 1 0 0 0-1-1zM81.167 11.549h-2.724a1 1 0 0 0-1 1v66.236a1 1 0 0 0 1 1h2.724a1 1 0 0 0 1-1V12.549a1 1 0 0 0-1-1zM51.875 11.549h-4a1 1 0 0 0-1 1v66.236a1 1 0 0 0 1 1h4a1 1 0 0 0 1-1V12.549a1 1 0 0 0-1-1zM42.167 11.549h-2.5a1 1 0 0 0-1 1v66.236a1 1 0 0 0 1 1h2.5a1 1 0 0 0 1-1V12.549a1 1 0 0 0-1-1zM73.523 11.549H71.98a1 1 0 0 0-1 1v66.236a1 1 0 0 0 1 1h1.543a1 1 0 0 0 1-1V12.549a1 1 0 0 0-1-1zM33.667 11.549h-4a1 1 0 0 0-1 1v66.236a1 1 0 0 0 1 1h4a1 1 0 0 0 1-1V12.549a1 1 0 0 0-1-1zM23.667 11.549h-1a1 1 0 0 0-1 1v66.236a1 1 0 0 0 1 1h1a1 1 0 0 0 1-1V12.549a1 1 0 0 0-1-1zM67.227 11.549h-.363c-.551 0-1 .448-1 1v66.236c0 .552.449 1 1 1h.363c.551 0 1-.448 1-1V12.549c0-.552-.45-1-1-1z"></path>
                    </svg>
                    <svg class="barcode" xmlns="http://www.w3.org/2000/svg">
                      <path d="M6.834 11.549H1a1 1 0 0 0-1 1v66.236a1 1 0 0 0 1 1h5.834a1 1 0 0 0 1-1V12.549a1 1 0 0 0-1-1zM62.043 11.549h-4.168a1 1 0 0 0-1 1v66.236a1 1 0 0 0 1 1h4.168a1 1 0 0 0 1-1V12.549a1 1 0 0 0-1-1zM17 11.549h-4a1 1 0 0 0-1 1v66.236a1 1 0 0 0 1 1h4a1 1 0 0 0 1-1V12.549a1 1 0 0 0-1-1zM90.334 11.549h-4a1 1 0 0 0-1 1v66.236a1 1 0 0 0 1 1h4a1 1 0 0 0 1-1V12.549a1 1 0 0 0-1-1zM81.167 11.549h-2.724a1 1 0 0 0-1 1v66.236a1 1 0 0 0 1 1h2.724a1 1 0 0 0 1-1V12.549a1 1 0 0 0-1-1zM51.875 11.549h-4a1 1 0 0 0-1 1v66.236a1 1 0 0 0 1 1h4a1 1 0 0 0 1-1V12.549a1 1 0 0 0-1-1zM42.167 11.549h-2.5a1 1 0 0 0-1 1v66.236a1 1 0 0 0 1 1h2.5a1 1 0 0 0 1-1V12.549a1 1 0 0 0-1-1zM73.523 11.549H71.98a1 1 0 0 0-1 1v66.236a1 1 0 0 0 1 1h1.543a1 1 0 0 0 1-1V12.549a1 1 0 0 0-1-1zM33.667 11.549h-4a1 1 0 0 0-1 1v66.236a1 1 0 0 0 1 1h4a1 1 0 0 0 1-1V12.549a1 1 0 0 0-1-1zM23.667 11.549h-1a1 1 0 0 0-1 1v66.236a1 1 0 0 0 1 1h1a1 1 0 0 0 1-1V12.549a1 1 0 0 0-1-1zM67.227 11.549h-.363c-.551 0-1 .448-1 1v66.236c0 .552.449 1 1 1h.363c.551 0 1-.448 1-1V12.549c0-.552-.45-1-1-1z"></path>
                    </svg>
                  </li>
                </ul>
              </div>
              :
              <center>
                <p className="no-ticket">No Tickets found</p></center>
            }
          </div>
          :
          null}
      </div>
      <center>
        <div class="booking-for" >
          <div class="col-md-4" >
            <div class="form-btn">
              <button class="submit-btn" ><Link to="/" className="continue-booking">Continue Booking</Link></button>
            </div>
          </div>
        </div>
      </center>
    </div>
  );
};

export default Ticket;
