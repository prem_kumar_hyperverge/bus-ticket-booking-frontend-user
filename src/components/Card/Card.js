import React from "react";
import {
  Col,
  Row,
  Card,
  CardBody,
  CardImg,
  Button,
} from "../../../node_modules/reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faBus,
  faCalendarAlt,
  faRupeeSign,
} from "@fortawesome/free-solid-svg-icons";
function CardDetails(props) {
  function openModal() {
    props.busDetails(
      props.value.seatDetails,
      props.value._id,
      props.value.fare
    );
  }
  return (
    <Row className="row">
      <Col md={12} sm={12} xs={12}>
        <Card className="flex-row" style={{ borderColor: "transparent" }}>
          <Col md={2} sm={2} xs={2}>
            {/* <CardImg
              className="card-img-left"
              src={props.value.busImage}
              style={{ width: "auto", height: 310 }}
            /> */}
          </Col>
          <Col md={8} sm={8} xs={8}>
            <CardBody>
              <Row>
                <Col lg={12} md={12} sm={12} xs={12}>
                  <Card className="card-style">
                    <CardBody>
                      <div className="cardbody-style">
                        <div className="left-div-font">
                          <FontAwesomeIcon
                            icon={faBus}
                            className="font-awesome-icons"
                          />{" "}
                          {props.value.busName}
                        </div>
                        <div className="side-div">
                          <FontAwesomeIcon
                            icon={faCalendarAlt}
                            className="font-awesome-icons"
                          />{" "}
                          {props.value.travelDate}
                        </div>
                        <br />
                        <br />
                        <div className="light-color">From</div>
                        <div className="dark-color">To</div>
                        <br />
                        <div className="font-float">
                          {props.value.sourceCity}
                        </div>
                        <div className="city-div">
                          {props.value.destinationCity}
                        </div>{" "}
                        <br />
                        <br />
                        <div className="light-color">Departure Time</div>
                        <div className="dark-color">Arrival Time</div>
                        <br />
                        <br />
                        <div className="font-float">
                          {props.value.departureTime}
                        </div>
                        <div className="city-div">
                          {props.value.arrivalTime}
                        </div>
                        <center>
                          <p>
                            {" "}
                            <FontAwesomeIcon
                              icon={faRupeeSign}
                              className="font-rupee"
                            />{" "}
                            <span className="fare-div">{props.value.fare}</span>
                          </p>
                        </center>
                        <div className="cardbody-style">
                          <center>
                            <div className="view-div">
                              <Button color="success" onClick={openModal}>
                                Book Seats
                              </Button>
                            </div>
                          </center>
                        </div>
                      </div>
                    </CardBody>
                  </Card>
                </Col>
              </Row>
            </CardBody>
          </Col>
        </Card>
      </Col>
    </Row>
  );
}
export default CardDetails;
