import React from "react";
import "./Details.css";
import { Col, Card, CardBody, CardText } from "reactstrap";

function Details(props) {
  function onChangeName(e) {
    props.onChangeName(e);
  }
  function onChangeEmail(e) {
    props.onChangeEmail(e);
  }
  function onChangeMobileNumber(e) {
    props.onChangeMobileNumber(e);
  }
  function genderChanged(e) {
    props.genderChanged(e);
  }
  function onChangeAge(e) {
    props.onChangeAge(e);
  }

  return (
    <Col xl={6} lg={6} md={12}>
      <Card className="amount-div">
        <CardBody>
          <center>
            <CardText>Seat No : {props.seats + ","}</CardText>
            <CardText>Amount : {props.fare}</CardText>
          </center>
        </CardBody>
      </Card>
      <Card className="passenger-div">
        <div class="booking-form">
          <Col xl={12} lg={12} md={12}>
            <div class="form-group">
              <span class="form-label">Name</span>
              <input
                class="form-control"
                type="text"
                placeholder="Name"
                value={props.name}
                onChange={onChangeName}
              />
            </div>
            <div class="form-group">
              <span class="form-label">Email</span>
              <input
                class="form-control"
                type="text"
                placeholder="Email"
                value={props.email}
                onChange={onChangeEmail}
              />
            </div>
            <div class="form-group">
              <span class="form-label">Mobile Number</span>
              <input
                class="form-control"
                type="text"
                placeholder="Mobile Number"
                value={props.mobileNumber}
                onChange={onChangeMobileNumber}
              />
            </div>
            <div class="form-group">
              <span class="form-label">Gender</span>
              <select
                class="form-control"
                value={props.gender}
                onChange={genderChanged}
              >
                <option value="Male">Male</option>
                <option value="Female">Female</option>
              </select>
            </div>
            <div class="form-group">
              <span class="form-label">Age</span>
              <input
                class="form-control"
                type="text"
                placeholder="Age"
                value={props.age}
                onChange={onChangeAge}
              />
            </div>
          </Col>
        </div>
      </Card>
    </Col>
  );
}
export default Details;
