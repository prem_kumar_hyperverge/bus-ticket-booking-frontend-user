import axios from "axios";
import {USER_URL} from '../constants/index'

const getBusDetails = (sourceCity,destinationCity,date) => {
  const body ={
    "sourceCity":sourceCity,
  "destinationCity":destinationCity,
  "travelDate":date,
  }
  return axios.post(USER_URL + "getBuses", body);
};

const busDetails = (busId) => {
  const body = {
    "busId":busId 
  }
  return axios.post(USER_URL + "busDetails", body);
};
const ticketDetails = (ticketId) => {
  const body = {
    "ticketId":ticketId 
  }
  return axios.post(USER_URL + "ticketDetails", body);
};

const bookSeats = (name, mobileNumber,email,age,gender,busId,seats) => {
  const body = { 
  "userName":name,
  "mobileNumber":mobileNumber,
  "email":email,
  "age":age,
  "gender":gender,
  "busId":busId,
  "seatId": seats
  };
  return axios.post(USER_URL + "bookSeats",body );
};

export default {
  getBusDetails,
  busDetails,
  ticketDetails,
  bookSeats
};