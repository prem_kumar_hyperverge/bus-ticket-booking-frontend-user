import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
} from "react-router-dom";
import Home from "./container/Home/Home.jsx";
import Ticket from "./container/Ticket/Ticket.jsx";
function App() {
  return (
    <Router>
      <Route exact path="/">
        <Home />
      </Route>
      <Route exact path="/ticketStatus">
        <Ticket />
      </Route>
    </Router>
  );
}
export default App;
